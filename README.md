# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | N         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | N         |
| Image tool                                       | 5%        | N         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use
1. Basic control tools
* Pencil and eraser   
* 可以經由調色盤調整pencil以及圖形的顏色
* Pencil，eraser的粗細由Radius左右的+,-來改變

2. Cursor icon
* 使用pencil和eraser時，cursor會改變

3. Refresh button
* 經由reset button來refresh canvas

4. Different brush shapes
* 可以點擊triangle，rectangle，circle來繪製想要的圖形

5. Download
* 點擊download來下載目前canvas上的圖形
    

### Function description

* Pencil，eraser以及改變筆刷粗細的部分，我參考了<https://www.youtube.com/watch?v=m4sioSqlXhQ&list=PLfdtiltiRHWHfOVfqI89Nc3xUMY-q-7f0&index=1>。
* 調色盤使用了input type="color"來完成，並且透過eventlistener來即時更新顏色。
* 為了確任目前使用的工具，我用mode這個變數來記錄，並且在切換工具的同時，改變cursor的樣式。
* 使用 context.clearRect(0, 0, canvas.width, canvas.height) 清除畫布上的圖形，以達到reset的效果。
* 在繪製圓形、長方形或是三角形時，為了讓畫布上只存在起始位置拖曳到目前位置的圖形，每次進入function時，需要先把前一次移動所產生的圖形清掉，直到mouseup。我的作法是先利用了canvas.width=canvas.width來清空整個畫布，再load之前存好的image，最後才更新圖形的座標位置。
* 按下download之後，"this.href" gets "canvas.toDataURL()"，就可以將目前畫布的圖案下載下來。
    
### Gitlab page link

    https://107062233.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    

<style>
table th{
    width: 100%;
}
</style>