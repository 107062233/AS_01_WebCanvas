var canvas=document.getElementById('mycanvas');
var context=canvas.getContext('2d');
var colorselecter=document.getElementById('colorselecter');
var radius=10;
var dragging=false;
var mode='';
var color='';
var start_x=0;
var start_y=0;
var prevImg=new Image();

canvas.width=window.innerWidth;
canvas.height=window.innerHeight;

function resetcanvas(){
    context.clearRect(0, 0, canvas.width, canvas.height);
    console.log('reset');
}

function changemode(name,cursorname){
    mode=name;
    console.log('mode:',name);
    document.body.style.cursor = cursorname;
    console.log('cursor style:',cursorname);
}

function updateradius(id){
    if(id=='plus_radius' && radius<30){
        radius+=4;
        console.log('radius+=4');
    }
    else if(id=='minus_radius' && radius>=5){
        radius-=4;
        console.log('radius-=4');
    }
    document.getElementById('radius').innerHTML=radius;
    console.log('current radius is ' ,radius);
}

function download(){
    var prev=window.location.href;
    var data=canvas.toDataURL();
    window.location.href=data.replace("image/png", "image/octet-stream");
    window.location.href=prev;
}

function changecolor(e){
    color=e.target.value;
    console.log('change color to:',e.target.value);
}

var clicked=function(e){
    console.log('clicked');
    dragging=true;
    start_x=e.offsetX;
    console.log('start_x:',start_x);
    start_y=e.offsetY;
    console.log('start_y:',start_y);
    prevImg.src=canvas.toDataURL();
}

var unclick=function(){
    console.log('unclick');
    dragging=false;
    context.beginPath();
}

var putPoint=function(e){
    if(dragging){
        if(mode=='eraser')
            context.globalCompositeOperation="destination-out";
        else
            context.globalCompositeOperation="source-over";
        if(mode=='pencil' || mode=='eraser'){
            context.lineWidth=radius*2;
            context.lineTo(e.offsetX,e.offsetY);
            context.strokeStyle=color;
            context.stroke();
            context.beginPath();
            context.arc(e.offsetX,e.offsetY,radius,0,Math.PI*2);
            context.fillStyle=color;
            context.fill();
            context.beginPath();
            context.moveTo(e.offsetX,e.offsetY);
        }
        else if(mode=='triangle'){
            canvas.width=canvas.width;
            context.drawImage(prevImg,0,0);
            context.lineWidth=3;
            context.beginPath();
            context.moveTo(start_x,start_y);
            context.lineTo(e.offsetX, e.offsetY);
            context.lineTo(start_x+(start_x-e.offsetX),e.offsetY);
            context.closePath();
            context.strokeStyle=color;
            context.stroke();
            context.moveTo(e.offsetX, e.offsetY);
            context.lineTo(start_x+(start_x-e.offsetX),e.offsetY);
            context.strokeStyle=color;
            context.stroke();
        }
        else if(mode=='rectangle'){
            canvas.width=canvas.width;
            context.drawImage(prevImg,0,0);
            context.lineWidth=3;
            context.strokeStyle=color;  
            context.strokeRect(start_x,start_y,e.offsetX-start_x,e.offsetY-start_y);
        }
        else if(mode=='circle'){
            canvas.width=canvas.width;
            context.drawImage(prevImg,0,0);
            context.lineWidth=3; 
            context.arc(start_x,start_y,Math.sqrt(Math.pow(e.offsetX-start_x,2)+Math.pow(e.offsetY-start_y,2)),0,Math.PI*2);
            context.strokeStyle=color; 
            context.stroke();
        }
    }
    
}

colorselecter.addEventListener("input",changecolor);
canvas.addEventListener('mousemove',putPoint);
canvas.addEventListener('mousedown',clicked);
canvas.addEventListener('mousedown',putPoint);
canvas.addEventListener('mouseup',unclick);